package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int diaSemana = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        //Calendar.Day_of_week retorna un nombre entre 1 i 7. Ens interesa entre 0 i 6.
        String[] diesSetmana = getResources().getStringArray(R.array.day_of_week);
        String salutacio = getString(R.string.hello) + "" + diesSetmana[diaSemana];

        Toast.makeText(this, salutacio, Toast.LENGTH_LONG).show();
        //Falta explicar com der l'aplicació multiidioma
    }
}
